#!/usr/bin/env zsh
source ~/.sexy.rc
sexy-dbg '#>>> zsh/init.zsh'

SEXY_ZSH="${SEXY_ROOT}/zsh"
SEXY_ZSH_DATA="${SEXY_ZSH}/data"

[[ ! -d "${SEXY_ZSH_DATA}" ]] && mkdir -p "${SEXY_ZSH_DATA}"

sexy-dbg '#<<< zsh/init.zsh'
